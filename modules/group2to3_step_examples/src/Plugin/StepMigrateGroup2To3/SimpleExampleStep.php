<?php

namespace Drupal\group2to3_step_examples\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @StepMigrateGroup2To3(
 *   id = "group2to3_step_examples_simple",
 *   label = @Translation("Example Simple"),
 *   dependency = "replace_new_bundle_group_relationship",
 * )
 */
class SimpleExampleStep extends StepPluginBase {

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->state = $container->get('state');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    $this->state->set('group2to3_step_examples_simple', 'Set a string');

    return self::FINISHED;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMessage(array &$sandbox, $progress) {
    return $this->t('A value has been created in the state service, which will be removed when uninstalling the module "group2to3_step_examples".');
  }
}
