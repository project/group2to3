<?php

namespace Drupal\group2to3_step_examples\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;

/**
 * @StepMigrateGroup2To3(
 *   id = "group2to3_step_examples_sandbox",
 *   label = @Translation("Example Sandbox"),
 * )
 */
class SandboxExampleStep extends StepPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    if (!isset($sandbox['group2to3_step_examples_sandbox'])) {
      $sandbox['group2to3_step_examples_sandbox'] = [
        'max' => 100,
        'progress' => 0,
      ];
    }

    $limit = 10;
    while ($limit) {
      $sandbox['group2to3_step_examples_sandbox']['progress']++;
      $limit--;
    }

    // Simulating the processing of a large amount of data.
    sleep(rand(1, 2));

    return $sandbox['group2to3_step_examples_sandbox']['progress'] / $sandbox['group2to3_step_examples_sandbox']['max'];
  }

  /**
   * {@inheritdoc}
   */
  protected function getMessage(array &$sandbox, $progress) {
    return $this->t('%progress of %max have been processed.', [
      '%progress' => $sandbox['group2to3_step_examples_sandbox']['progress'],
      '%max' => $sandbox['group2to3_step_examples_sandbox']['max'],
    ]);
  }
}
