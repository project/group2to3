# Group 2 to 3 upgrade

## Introduction

The module should only be used to upgrade the group module from major version 2 to major version 3.

Once executed, this module must be uninstalled and removed.

> It is recommended to use `drush` and clearly always make a backup before executing the process.

## Instructions

1. Have the latest version of group 2.x installed
2. It is recommended to have the configurations fully synchronized
3. Install this module (`drush en group2to3`)
4. Replace the group module from version 2.x to 3.x (`composer require drupal/group:^3.0`)
5. Execute `drush updb`. Several steps will be executed.
6. If all goes well, export the resulting configurations: `drush config:export`
7. Test the update

## Step Plugin

This module has a Plugin system to define several steps for the migration process. Currently, the defined steps are:

1. Migrate configurations from Group Content to Group Relationship (including types and their fields)
2. Create the new tables and migrate the data
3. Perform cleanup of the previous version
4. Update Views using group_content to group_relationships

New steps can be created if you have additional and custom configurations related to the Group module that need to be
included in the migration process.

These steps are defined in the `Plugin/StepMigrateGroup2To3` folder and you will need to extend the
`\Drupal\group2to3\MigrateGroup2To3\StepPluginBase` class. See as an example how the `group2to3_step_example` module
implements it
