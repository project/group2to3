<?php

namespace Drupal\group2to3\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;
use Drupal\group2to3\MigrateGroup2To3\UpgradeInterface;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @StepMigrateGroup2To3(
 *   id = "remove_old_configurations",
 *   label = @Translation("Remove old configurations"),
 *   dependency = "update_field_entity_reference_target_type_configuration",
 * )
 */
class RemoveOldConfigurations extends StepPluginBase {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    foreach ($this->entityFieldManager->getFieldStorageDefinitions(UpgradeInterface::OLD_ENTITY_TYPE_ID) as $field_storage) {
      if ($field_storage instanceof FieldStorageConfigInterface) {
        $field_storage->delete();
      }
    }

    /** @var \Drupal\group2to3\MigrateGroup2To3\GroupContentType $group_content_type */
    foreach ($this->entityTypeManager->getStorage('group_content_type')->loadMultiple() as $group_content_type) {
      $group_content_type->delete();
    }

    return self::FINISHED;
  }

}
