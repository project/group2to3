<?php

namespace Drupal\group2to3\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;
use Drupal\group2to3\MigrateGroup2To3\UpgradeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @StepMigrateGroup2To3(
 *   id = "update_views_configuration",
 *   label = @Translation("Update views configuration"),
 *   dependency = "update_entity_definition_installed",
 * )
 */
class UpdateViewsConfiguration extends StepPluginBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    /** @var \Drupal\views\ViewEntityInterface[] $views */
    $views = $this->entityTypeManager->getStorage('view')->loadMultiple();
    $replaces = [
      'group_content_plugins' => 'group_relation_plugins',
      UpgradeInterface::OLD_ENTITY_TYPE_ID => UpgradeInterface::NEW_ENTITY_TYPE_ID,
    ];
    foreach ($views as $view) {
      $display = $view->get('display');

      $this->replaceKeysAndValues($display, $replaces, $changes);
      if ($changes) {
        $view->set('display', $display);
        $view->save();
      }
    }

    return self::FINISHED;
  }

}
