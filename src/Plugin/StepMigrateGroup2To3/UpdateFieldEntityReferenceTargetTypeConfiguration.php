<?php

namespace Drupal\group2to3\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;
use Drupal\group2to3\MigrateGroup2To3\UpgradeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This step proceeds to update the entity_reference and
 * dynamic_entity_reference fields that were targeting the group_content or
 * group_content_type entities.
 *
 * @StepMigrateGroup2To3(
 *   id = "update_field_entity_reference_target_type_configuration",
 *   label = @Translation("Updates the target type of Entity Reference fields"),
 *   dependency = "replace_new_bundle_group_relationship",
 * )
 */
class UpdateFieldEntityReferenceTargetTypeConfiguration extends StepPluginBase {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    $field_storage_config_storage = $this->entityTypeManager->getStorage('field_storage_config');

    $replace_entity_types = [
      UpgradeInterface::OLD_ENTITY_TYPE_ID => UpgradeInterface::NEW_ENTITY_TYPE_ID,
      UpgradeInterface::OLD_ENTITY_TYPE_ID . '_type' => UpgradeInterface::NEW_ENTITY_TYPE_ID . '_type',
    ];

    /** @var \Drupal\field\FieldStorageConfigInterface $field_storage_config */
    foreach ($field_storage_config_storage->loadMultiple() as $field_storage_config) {
      if ($field_storage_config->getType() == 'entity_reference') {
        $old_target_type = $field_storage_config->getSetting('target_type');
        if (in_array($old_target_type, array_keys($replace_entity_types))) {
          $new_target_type = $replace_entity_types[$old_target_type];
          $field_storage_config->setSetting('target_type', $new_target_type);
          $field_storage_config->save();
        }
      }
      elseif ($field_storage_config->getType() == 'dynamic_entity_reference') {
        $old_target_types_id = $field_storage_config->getSetting('entity_type_ids');
        $new_target_types_id = $old_target_types_id;
        foreach ($old_target_types_id as $old_target_type) {
          if (in_array($old_target_type, array_keys($replace_entity_types))) {
            $new_target_types_id[$replace_entity_types[$old_target_type]] = $replace_entity_types[$old_target_type];
            unset($new_target_types_id[$old_target_type]);
          }
        }

        if ($old_target_types_id != $new_target_types_id) {
          $field_storage_config->setSetting('entity_type_ids', $new_target_types_id);
          $field_storage_config->save();

          // The dynamic entity reference stores the entity type in a column.
          // This needs to be updated.
          // Perhaps this could be executed in another step.
          /** @var \Drupal\Core\Entity\Sql\TableMappingInterface $table_mapping */
          $table_mapping = $this->entityTypeManager->getStorage($field_storage_config->getTargetEntityTypeId())->getTableMapping();
          $data_table_field_name = $table_mapping->getDedicatedDataTableName($field_storage_config);
          $revision_table_field_name = $table_mapping->getDedicatedRevisionTableName($field_storage_config);
          foreach ($replace_entity_types as $old => $new) {
            $this->database->update($data_table_field_name)
              ->fields([
                $field_storage_config->getName() . '_target_type' => $new,
              ])
              ->condition($field_storage_config->getName() . '_target_type', $old)
              ->execute();

            $this->database->update($revision_table_field_name)
              ->fields([
                $field_storage_config->getName() . '_target_type' => $new,
              ])
              ->condition($field_storage_config->getName() . '_target_type', $old)
              ->execute();
          }
        }
      }
    }

    $field_config_storage = $this->entityTypeManager->getStorage('field_config');
    /** @var \Drupal\Core\Field\FieldConfigInterface $field_config */
    foreach ($field_config_storage->loadMultiple() as $field_config) {
      if ($field_config->getType() == 'entity_reference') {
        $old_target_type = $field_config->getSetting('target_type');
        if (in_array($old_target_type, array_keys($replace_entity_types))) {
          $new_target_type = $replace_entity_types[$old_target_type];
          $field_config->setSetting('handler', str_replace('default:' . $old_target_type, 'default:' . $new_target_type, $field_config->getSetting('handler')));
          $field_config->save();
        }
      }
      elseif ($field_config->getType() == 'dynamic_entity_reference') {
        $settings = $field_config->getSettings();
        foreach ($replace_entity_types as $old => $new) {
          if (isset($settings[$old])) {
            $settings[$new] = $settings[$old];
            $settings[$new]['handler'] = str_replace('default:' . $old, 'default:' . $new, $settings[$new]['handler']);
            unset($settings[$old]);
          }
        }
        $field_config->setSettings($settings);
        $field_config->save();
      }
    }

    return self::FINISHED;
  }

}
