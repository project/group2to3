<?php

namespace Drupal\group2to3\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;
use Drupal\group2to3\MigrateGroup2To3\UpgradeInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @StepMigrateGroup2To3(
 *   id = "copy_configurations",
 *   label = @Translation("Copy Configurations"),
 * )
 */
class CopyConfigurations extends StepPluginBase {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    if (!isset($sandbox['bundles_mapping'])) {
      $sandbox['bundles_mapping'] = [];
    }
    $new_field_storage_definitions = [];
    $field_storage_config_storage = $this->entityTypeManager->getStorage('field_storage_config');
    foreach ($this->entityFieldManager->getFieldStorageDefinitions(UpgradeInterface::OLD_ENTITY_TYPE_ID) as $field_storage) {
      if ($field_storage instanceof FieldStorageConfigInterface) {
        $field_name = $field_storage->getName();
        $new_id = UpgradeInterface::NEW_ENTITY_TYPE_ID . '.' . $field_name;
        if ($already_exists = $field_storage_config_storage->load($new_id)) {
          $already_exists->delete();
        }
        $new_field_storage = clone $field_storage;
        $new_field_storage->enforceIsNew();
        $new_field_storage->set('entity_type', UpgradeInterface::NEW_ENTITY_TYPE_ID);
        $new_field_storage->setOriginalId($new_id);
        $new_field_storage->set('id', $new_id);
        $new_field_storage->set('uuid', $this->buildANewUuid($new_field_storage->uuid(), $new_id));
        $new_field_storage->save();
        $new_field_storage_definitions[$new_field_storage->getName()] = $new_field_storage;
      }
    }
    /** @var \Drupal\group\Entity\Storage\GroupRelationshipTypeStorageInterface $group_relationship_storage */
    $group_relationship_storage = $this->entityTypeManager->getStorage('group_relationship_type');
    /** @var \Drupal\group2to3\MigrateGroup2To3\GroupContentType[] $group_content_types */
    $group_content_types = $this->entityTypeManager->getStorage('group_content_type')->loadMultiple();
    foreach ($group_content_types as $group_content_type) {
      $group_type = $group_content_type->get('group_type');
      $plugin_id = $group_content_type->get('content_plugin');
      $new_id = $group_relationship_storage->getRelationshipTypeId($group_type, $plugin_id);
      if ($already_exists = $group_relationship_storage->load($new_id)) {
        $already_exists->delete();
      }
      $group_relationship_type = $group_relationship_storage->create($group_content_type->toArray());
      $sandbox['bundles_mapping'][$group_content_type->id()] = $new_id;
      if ($new_id != $group_relationship_type->id()) {
        $group_relationship_type->setOriginalId($new_id);
        $group_relationship_type->set('id', $new_id);
      }
      $group_relationship_type->save();

      foreach ($new_field_storage_definitions as $field_storage) {
        /** @var \Drupal\field\FieldConfigInterface $field_config */
        $field_config = FieldConfig::loadByName('group_content', $group_content_type->id(), $field_storage->getName());
        if (!$field_config) {
          continue;
        }
        $new_field_config = clone $field_config;
        $field_name = $field_storage->getName();
        $new_id = UpgradeInterface::NEW_ENTITY_TYPE_ID . '.' . $group_relationship_type->id() . '.' . $field_name;
        $new_field_config->enforceIsNew();
        $new_field_config->set('id', $new_id);
        $new_field_config->setOriginalId($new_id);
        $new_field_config->set('bundle', $group_relationship_type->id());
        $new_field_config->set('entity_type', UpgradeInterface::NEW_ENTITY_TYPE_ID);
        $new_field_config->set('uuid', $this->buildANewUuid($new_field_config->uuid(), $new_id));
        try {
          $new_field_config->save();
        } catch (EntityStorageException $e) {
        }
      }
    }

    return self::FINISHED;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMessage(array &$sandbox, $progress) {
    return $this->t('Configuration copy complete.');
  }
}
