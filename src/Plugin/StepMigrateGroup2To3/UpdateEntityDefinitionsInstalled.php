<?php

namespace Drupal\group2to3\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;
use Drupal\group2to3\MigrateGroup2To3\UpgradeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @StepMigrateGroup2To3(
 *   id = "update_entity_definition_installed",
 *   label = @Translation("Update entity definition installed"),
 *   dependency = "remove_old_configurations",
 * )
 */
class UpdateEntityDefinitionsInstalled extends StepPluginBase {

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->keyValueFactory = $container->get('keyvalue');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    $entity_definitions_installed = $this->keyValueFactory->get('entity.definitions.installed');
    $entity_definitions_installed->set(UpgradeInterface::NEW_ENTITY_TYPE_ID . '.entity_type', $this->entityTypeManager->getDefinition(UpgradeInterface::NEW_ENTITY_TYPE_ID));
    $entity_definitions_installed->set(UpgradeInterface::NEW_ENTITY_TYPE_ID . '_type.entity_type', $this->entityTypeManager->getDefinition(UpgradeInterface::NEW_ENTITY_TYPE_ID . '_type'));

    $field_storage_definitions  = $this->entityFieldManager->getFieldStorageDefinitions(UpgradeInterface::NEW_ENTITY_TYPE_ID);
    $entity_definitions_installed->set('group_relationship.field_storage_definitions', $field_storage_definitions);

    $entity_definitions_installed->deleteMultiple([
      UpgradeInterface::OLD_ENTITY_TYPE_ID . '.entity_type',
      UpgradeInterface::OLD_ENTITY_TYPE_ID . '.field_storage_definitions',
      UpgradeInterface::OLD_ENTITY_TYPE_ID . '_type.entity_type',
    ]);

    $entity_storage_schema_sql = $this->keyValueFactory->get('entity.storage_schema.sql');

    $schema = $entity_storage_schema_sql->getAll();
    foreach ($schema as $name => $value) {
      if (strpos($name, UpgradeInterface::OLD_ENTITY_TYPE_ID . '.') === 0) {
        $new_name = str_replace(UpgradeInterface::OLD_ENTITY_TYPE_ID . '.', UpgradeInterface::NEW_ENTITY_TYPE_ID . '.', $name);
        if (!$entity_storage_schema_sql->get($new_name)) {
          $replaces = [
            UpgradeInterface::OLD_ENTITY_TYPE_ID => UpgradeInterface::NEW_ENTITY_TYPE_ID,
          ];
          $this->replaceKeysAndValues($value, $replaces, $changes);
          $entity_storage_schema_sql->set($new_name, $value);
        }
        $entity_storage_schema_sql->delete($name);
      }
    }

    return self::FINISHED;
  }

}
