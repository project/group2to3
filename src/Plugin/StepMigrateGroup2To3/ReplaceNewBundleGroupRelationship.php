<?php

namespace Drupal\group2to3\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @StepMigrateGroup2To3(
 *   id = "replace_new_bundle_group_relationship",
 *   label = @Translation("Replace new bundle in group relationship table"),
 *   dependency = "copy_data",
 * )
 */
class ReplaceNewBundleGroupRelationship extends StepPluginBase {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    foreach ($sandbox['bundles_mapping'] as $old_bundle => $new_bundle) {
      $this->database->update('group_relationship')
        ->fields([
          'type' => $new_bundle,
        ])
        ->condition('type', $old_bundle)
        ->execute();
      $this->database->update('group_relationship_field_data')
        ->fields([
          'type' => $new_bundle,
        ])
        ->condition('type', $old_bundle)
        ->execute();
    }

    return self::FINISHED;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMessage(array &$sandbox, $progress) {
    return $this->t('Replacement completed');
  }

}
