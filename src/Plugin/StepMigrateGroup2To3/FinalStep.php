<?php

namespace Drupal\group2to3\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;

/**
 * @StepMigrateGroup2To3(
 *   id = "final_step",
 *   label = @Translation("Final Step"),
 * )
 */
class FinalStep extends StepPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    return self::FINISHED;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMessage(array &$sandbox, $progress) {
    return $this->t('Congratulations, you upgraded the module Group!. Now you can uninstall this module.');
  }
}
