<?php

namespace Drupal\group2to3\Plugin\StepMigrateGroup2To3;

use Drupal\group2to3\MigrateGroup2To3\StepPluginBase;
use Drupal\group2to3\MigrateGroup2To3\UpgradeInterface;
use Drupal\Core\Utility\UpdateException;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @StepMigrateGroup2To3(
 *   id = "copy_data",
 *   label = @Translation("Copy data"),
 *   dependency = "copy_configurations",
 * )
 */
class CopyData extends StepPluginBase {
  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array &$sandbox) {
    if (!isset($sandbox['data_to_migrate'])) {
      $sandbox['data_to_migrate'] = $this->getTotalDataToMigrate();
    }
    if (!$sandbox['data_to_migrate']['total']) {
      $sub_progress = self::FINISHED;
    }
    else {
      foreach ($sandbox['data_to_migrate']['fields'] as $field_name => &$data) {
        if ($data['progress'] == $data['total']) {
          continue;
        }

        $limit = 10;
        $old_table_name = $this->getTableNameOfField($field_name, UpgradeInterface::OLD_ENTITY_TYPE_ID);
        $results = $this->database->select($old_table_name, 't')
          ->fields('t')
          ->range($data['progress'], $limit)
          ->execute();

        $new_table_name = $this->getTableNameOfField($field_name, UpgradeInterface::NEW_ENTITY_TYPE_ID);

        foreach ($results as $result) {
          $values = (array) $result;
          $values['bundle'] = $sandbox['bundles_mapping'][$values['bundle']];
          $this->database->insert($new_table_name)
            ->fields($values)
            ->execute();
          $data['progress']++;
          $sandbox['data_to_migrate']['progress']++;
        }
        break;
      }

      $sub_progress = $sandbox['data_to_migrate']['progress'] / $sandbox['data_to_migrate']['total'];
    }

    return $sub_progress;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMessage(array &$sandbox, $progress) {
    if ($progress >= 1) {
      unset($sandbox['data_to_migrate']);
      return $this->t('Copied data. Processed to save the new configurations.');
    }
    return $this->t('Copying data %progress%.', [
      '%progress' => round($progress * 100),
    ]);
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTotalDataToMigrate() {
    $data_to_migrate = [
      'fields' => [],
      'total' => 0,
      'progress' => 0,
    ];
    $field_storages = $this->entityFieldManager->getFieldStorageDefinitions(UpgradeInterface::OLD_ENTITY_TYPE_ID);

    /** @var \Drupal\Core\Entity\Sql\TableMappingInterface $table_mapping */
    $table_mapping = $this->entityTypeManager->getStorage(UpgradeInterface::OLD_ENTITY_TYPE_ID)->getTableMapping();
    foreach ($field_storages as $field_storage) {
      if ($field_storage instanceof FieldStorageConfigInterface) {
        $table_name = $table_mapping->getDedicatedDataTableName($field_storage);
        $count = $this->database->select($table_name, 't')
          ->countQuery()
          ->execute()
          ->fetchField();
        $data_to_migrate['fields'][$field_storage->getName()] = [
          'total' => $count,
          'progress' => 0,
        ];
        $data_to_migrate['total'] += $count;
      }
    }

    return $data_to_migrate;
  }

  /**
   * @param $field_name
   * @param $entity_type_id
   *
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Utility\UpdateException
   */
  protected function getTableNameOfField($field_name, $entity_type_id) {
    $field_storages = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);
    if (!isset($field_storages[$field_name])) {
      throw new UpdateException($field_name . ' does not exists.');
    }

    /** @var \Drupal\Core\Entity\Sql\TableMappingInterface $table_mapping */
    $table_mapping = $this->entityTypeManager->getStorage($entity_type_id)->getTableMapping();

    return $table_mapping->getDedicatedDataTableName($field_storages[$field_name]);
  }
}
