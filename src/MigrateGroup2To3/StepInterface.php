<?php

namespace Drupal\group2to3\MigrateGroup2To3;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Interface for step_migrate_2_to_3 plugins.
 */
interface StepInterface extends ContainerFactoryPluginInterface {

  const FINISHED = 1;

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * @param array $sandbox
   *
   * @return array
   *   - message
   *   - progress
   */
  public function execute(array &$sandbox);

}
