<?php

namespace Drupal\group2to3\MigrateGroup2To3;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Extension\Exception\UnknownExtensionException;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\UpdateException;

class Upgrade implements UpgradeInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\group2to3\MigrateGroup2To3\StepPluginManager
   */
  protected $stepPluginManager;

  /**
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * @param \Drupal\group2to3\MigrateGroup2To3\StepPluginManager $step_plugin_manager
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entity_definition_update_manager
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   */
  public function __construct(StepPluginManager $step_plugin_manager, EntityDefinitionUpdateManagerInterface $entity_definition_update_manager, ModuleExtensionList $module_extension_list) {
    $this->stepPluginManager = $step_plugin_manager;
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
    $this->moduleExtensionList = $module_extension_list;
  }

  /**
   * {@inheritdoc}
   */
  public function doUpgrade(array &$sandbox) {
    if (!isset($sandbox['steps_status'])) {
      $this->checkRequirementsMetForUpgrade();
      $sandbox['steps_status'] = [];
      foreach ($this->stepPluginManager->getStepsOrdered() as $id => $definition) {
        $sandbox['steps_status'][$id] = 0;
      }
    }

    $current_step = NULL;
    $step_number = 0;
    foreach ($sandbox['steps_status'] as $id => $progress) {
      $step_number++;
      if ($progress == StepInterface::FINISHED) {
        continue;
      }
      $current_step = $id;
      break;
    }

    if (is_null($current_step)) {
      return $this->t('There are no more steps to execute.');
    }

    /** @var \Drupal\group2to3\MigrateGroup2To3\StepInterface $step */
    $step = $this->stepPluginManager->createInstance($current_step, [
      'steps_status' => $sandbox['steps_status'],
    ]);

    $result = $step->execute($sandbox);

    $sandbox['steps_status'][$current_step] = $result['progress'];

    $sandbox['#finished'] = $this->getSumStepsProgress($sandbox['steps_status']) / count($sandbox['steps_status']);

    return $this->t('Step %step_number of %total (%step): %message', [
      '%step_number' => $step_number,
      '%total' => count($sandbox['steps_status']),
      '%step' => $step->label(),
      '%message' => $result['message'],
    ]);
  }

  /**
   * @param $steps_status
   *
   * @return int
   */
  protected function getSumStepsProgress($steps_status) {
    $total = 0;
    foreach ($steps_status as $step_status) {
      $total += $step_status;
    }
    return $total;
  }

  /**
   * Check if the requirements to perform the upgrade are met
   *
   * @throws \Drupal\Core\Utility\UpdateException
   */
  protected function checkRequirementsMetForUpgrade() {
    if (!$this->inGroup3()) {
      throw new UpdateException('Cannot find module group 3.x.');
    }
    if (!$this->canUpdate()) {
      throw new UpdateException('You can no longer run this update. This is because the Group Relationship entity type is already installed. If this is the case, uninstall this module.');
    }
  }

  /**
   * Check if Group module is positioned in version 3.x
   *
   * @return bool
   * @throws \Drupal\Core\Utility\UpdateException
   */
  protected function inGroup3() {
    try {
      $group_module = $this->moduleExtensionList->reset()->get('group');

      $info = $group_module->info ?? [];
      $version = $info['version'] ?? NULL;

      if ($version && version_compare($version, '3') >= 0) {
        return TRUE;
      }
    }
    catch (UnknownExtensionException $e) {
      throw new UpdateException('The group module is not installed');
    }

    return FALSE;
  }

  /**
   * Check if it can be updated.
   *
   * @return bool
   */
  protected function canUpdate() {
    return !$this->entityDefinitionUpdateManager->getEntityType('group_relationship');
  }
}
