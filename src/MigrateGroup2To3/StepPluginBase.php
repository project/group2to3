<?php

namespace Drupal\group2to3\MigrateGroup2To3;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for step_migrate_2_to_3 plugins.
 */
abstract class StepPluginBase extends PluginBase implements StepInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * @param array $sandbox
   *
   * @return float
   *   between 0 and 1.
   */
  abstract protected function doExecute(array &$sandbox);

  /**
   * @param array $sandbox
   * @param $progress
   *
   * @return \Drupal\Component\Render\MarkupInterface
   */
  protected function getMessage(array &$sandbox, $progress) {
    return $this->t('Process finished.');
  }

  /**
   * {@inheritdoc}
   */
  public function execute(array &$sandbox) {
    $progress = $this->doExecute($sandbox);
    if ($progress > 1) {
      $progress = self::FINISHED;
    }

    return [
      'message' => $this->getMessage($sandbox, $progress),
      'progress' => $progress,
    ];
  }

  /**
   * Helper to build a new uuid using a reference name.
   *
   * @param $from_uuid
   * @param $reference_name
   *
   * @return string
   */
  protected function buildANewUuid($from_uuid, $reference_name) {
    $new_uuid_part = substr(md5($reference_name), 0, 12);
    return substr($from_uuid, 0, -12) . $new_uuid_part;
  }

  /**
   * @param array $array
   * @param array $replaces
   * @param bool $changes
   *
   * @return void
   */
  protected function replaceKeysAndValues(array &$array, array $replaces, &$changes = FALSE) {
    foreach ($array as $key => &$value) {
      $new_key = $this->replaceStringFromReplaces($key, $replaces);
      if (is_array($value)) {
        $this->replaceKeysAndValues($value, $replaces, $changes);
      }
      else {
        $new_value = $this->replaceStringFromReplaces($value, $replaces);
        if ($new_value != $value) {
          $changes = TRUE;
          $value = $new_value;
        }
      }
      if ($new_key != $key) {
        $changes = TRUE;
        $array = $this->replaceKeyArray($array, $key, $new_key);
      }
    }
  }

  /**
   * @param $string
   * @param array $replaces
   *
   * @return array|mixed|string|string[]
   */
  protected function replaceStringFromReplaces($string, array $replaces) {
    foreach ($replaces as $from => $to) {
      if ($string == $from) {
        return $to;
      }

      if (strpos($string, $from) !== FALSE) {
        return str_replace($from, $to, $string);
      }
    }
    return $string;
  }

  /**
   * @param array $array
   * @param $old_key
   * @param $new_key
   *
   * @return array|false
   */
  function replaceKeyArray(array $array, $old_key, $new_key) {
    if (array_key_exists($old_key, $array)) {
      $keys = array_keys($array);
      $keys[array_search($old_key, $keys)] = $new_key;

      return array_combine($keys, $array);
    }
    return $array;
  }
}
