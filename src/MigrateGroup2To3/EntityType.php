<?php

namespace Drupal\group2to3\MigrateGroup2To3;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Update\UpdateHookRegistry;

class EntityType implements EntityTypeInterface {

  /**
   * @var \Drupal\Core\Update\UpdateHookRegistry
   */
  protected $updateHookRegistry;

  /**
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * @param \Drupal\Core\Update\UpdateHookRegistry $update_hook_registry
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entity_definition_update_manager
   */
  public function __construct(UpdateHookRegistry $update_hook_registry, EntityDefinitionUpdateManagerInterface $entity_definition_update_manager) {
    $this->updateHookRegistry = $update_hook_registry;
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function alter(array &$entity_types) {
    $schema = $this->updateHookRegistry->getInstalledVersion('group2to3');
    if ($schema < UpgradeInterface::SCHEMA_UPDATED && isset($entity_types['group_relationship_type'])) {
      $new_entity_type_installed = $this->entityDefinitionUpdateManager->getEntityType('group_relationship');
      if (!$new_entity_type_installed) {
        /** @var \Drupal\Core\Config\Entity\ConfigEntityTypeInterface $clone */
        $clone = clone $entity_types['group_relationship_type'];
        $clone->set('id', 'group_content_type');
        $clone->set('config_prefix', 'content_type');
        $clone->set('bundle_of', 'group_content');
        $clone->setClass('Drupal\group2to3\MigrateGroup2To3\GroupContentType');
        $clone->set('originalClass', 'Drupal\group2to3\MigrateGroup2To3\GroupContentType');
        $entity_types['group_content_type'] = $clone;

        /** @var \Drupal\Core\Entity\ContentEntityTypeInterface $clone */
        $clone = clone $entity_types['group_relationship'];
        $clone->set('bundle_entity_type', 'group_content_type');
        $clone->setClass('Drupal\group2to3\MigrateGroup2To3\GroupContent');
        $clone->set('originalClass', 'Drupal\group2to3\MigrateGroup2To3\GroupContent');
        $clone->set('id', 'group_content');
        $entity_types['group_content'] = $clone;
      }
    }
  }

}
