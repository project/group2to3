<?php

namespace Drupal\group2to3\MigrateGroup2To3;

interface EntityTypeInterface {

  /**
   * When switching from 2.x to 3.x, the group_content* entity types no longer
   * exist. They must be temporarily redefined during the migration.
   * @param array $entity_types
   *
   * @return void
   *
   * @see group2to3_entity_type_alter().
   */
  public function alter(array &$entity_types);
}
