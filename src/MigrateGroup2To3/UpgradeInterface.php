<?php

namespace Drupal\group2to3\MigrateGroup2To3;

interface UpgradeInterface {

  const OLD_ENTITY_TYPE_ID = 'group_content';

  const NEW_ENTITY_TYPE_ID = 'group_relationship';

  const SCHEMA_UPDATED = 9001;

  /**
   * @param array $sandbox
   *
   * @see \group2to3_update_9001().
   *
   * @throws \Drupal\Core\Utility\UpdateException
   */
  public function doUpgrade(array &$sandbox);
}
