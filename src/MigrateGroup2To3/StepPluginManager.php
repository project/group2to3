<?php

namespace Drupal\group2to3\MigrateGroup2To3;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * StepMigrate2To3 plugin manager.
 */
class StepPluginManager extends DefaultPluginManager {

  /**
   * Constructs StepMigrate2To3PluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/StepMigrateGroup2To3',
      $namespaces,
      $module_handler,
      'Drupal\group2to3\MigrateGroup2To3\StepInterface',
      'Drupal\group2to3\Annotation\StepMigrateGroup2To3'
    );
    $this->alterInfo('step_migrate_group_2to3_info');
    $this->setCacheBackend($cache_backend, 'step_migrate_group_2to3_plugins');
  }

  /**
   * @return array
   */
  public function getStepsOrdered() {
    $definitions = $this->getDefinitions();
    $sorted = [];
    while ($count = count($definitions)) {
      foreach ($definitions as $id => $definition) {
        if (isset($definition['dependency'])) {
          $dep = $definition['dependency'];
          if (isset($sorted[$dep])) {
            unset($definitions[$id]['dependency']);
          }
        }
      }
      foreach ($definitions as $id => $definition) {
        if (!isset($definition['dependency'])) {
          $sorted[$id] = $definition;
          unset($definitions[$id]);
        }
      }
      if (count($definitions) == $count) {
        throw new \InvalidArgumentException('Unresolvable dependency');
      }
    }

    // Make sure the final_step plugin is at the end
    $definition = $sorted['final_step'];
    unset($sorted['final_step']);
    $sorted['final_step'] = $definition;

    return $sorted;
  }
}
