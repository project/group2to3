<?php

namespace Drupal\group2to3\MigrateGroup2To3;

use Drupal\group\Entity\GroupRelationshipType;

/**
 * Temporary use for migration.
 *
 * @see \Drupal\group2to3\MigrateGroup2To3\EntityTypeInterface::alter()
 */
class GroupContentType extends GroupRelationshipType {}
