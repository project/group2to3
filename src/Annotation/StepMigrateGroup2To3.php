<?php

namespace Drupal\group2to3\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines step_migrate_2_to_3 annotation object.
 *
 * @Annotation
 */
class StepMigrateGroup2To3 extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * @var string
   */
  public $dependency;
}
